import json
import requests


class GLClient(object):
    def __init__(self, instance_url=None, access_token=None):
        self.base_url = f"https://gitlab.com/api/v4"
        self.access_token = access_token
        if access_token is not None:
            user_data = verify_credentials(self.base_url, access_token)
            credential_data = user_data.json()
            self.user_id = credential_data["id"]
            self.user_name = credential_data["username"]

    def get_followers(self, user_id=None):
        if user_id is None:
            user_id = self.user_id
        url = f"{self.base_url}/accounts/{user_id}/followers"
        headers = {"Authorization": f"Bearer {self.access_token}"}
        return requests.request("GET", url, headers=headers, data={}).json()

    def get_following(self, user_id=None):
        if user_id is None:
            user_id = self.user_id
        url = f"{self.base_url}/accounts/{user_id}/following"
        headers = {"Authorization": f"Bearer {self.access_token}"}
        return requests.request("GET", url, headers=headers, data={}).json()

    def disable_reblogs(self, target_id=None):
        if target_id is None:
            raise ValueError('A target user ID must be provided')
        url = f"{self.base_url}/accounts/{target_id}/follow"
        data = {"reblogs": False}
        headers = {"Authorization": f"Bearer {self.access_token}"}
        return requests.request("GET", url, headers=headers, data=data)


def verify_credentials(base_url, access_token):
    url = f"{base_url}/user"
    headers = {"Authorization": f"Bearer {access_token}"}
    return requests.request("GET", url, headers=headers, data={})
