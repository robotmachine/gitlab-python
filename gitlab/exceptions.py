class GLClientError(Exception):
    def __init__(self, resource, msg):
        self.resource = resource
        self.msg = msg
