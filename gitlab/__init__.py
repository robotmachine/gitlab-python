import logging

from gitlab.client import GLClient
from gitlab.exceptions import GLClientError

logging.getLogger(__name__).addHandler(logging.NullHandler())